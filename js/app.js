var listIDTime = ['hour', 'minute', 'day', 'month', 'year'];
var companyInfo = {};
var origin = window.location.origin;
//Icon loading khi tải trang web
$(window).on("load", function () {
// Animate loader off screen
    $("body > .se-pre-con").fadeOut("slow");
    setYear();
});
function setYear() {
    var now = new Date();
    $('#year').append('<option value="' + now.getFullYear() + '">' + now.getFullYear() + '</option>');
    $('#year').append('<option value="' + (now.getFullYear() + 1) + '">' + (now.getFullYear() + 1) + '</option>');
}
$('.form-group.table select').click(function () {
    if ($('#restaurant').val() == '') {
        alert("Please Choose a Restaurant!");
        return;
    }
});
//Ẩn select option
$('select').on('mousedown', function (e) {
    e.preventDefault();
    this.blur();
    window.focus();
});
//Hiển thị popup
function popup(selector, text) {
    $(selector).find('.modal-body').html(text);
    $(selector).modal('show');
}
;
//Hiện select option khi click vào arrow
$('.select-arrow').click(function () {
    var target = '#' + $(this).attr('data-select');
    var $target = $('#' + $(this).attr('data-select'));
    if ($target.attr("disabled")) {
        return;
    }
    var $clone = $target.clone().removeAttr('id');
    $clone.val($target.val()).css({
        overflow: "auto",
        position: 'absolute',
        'z-index': 999,
        background: "#fff8dc",
        border: "1px solid #999966",
        left: $target.offset().left,
        height: "auto",
        top: $target.offset().top + $target.outerHeight(),
        width: $target.outerWidth()
    }).attr('size', $clone.find('option').length > 10 ? 10 : $clone.find('option').length).change(function () {
        $target.val($clone.val());
        $target.find('option').removeAttr('selected');
        $(target + ' option[value="' + $clone.val() + '"]').attr("selected", "true");
        if ($target.attr('id') == 'restaurant') {
            getCompanyData();
        }
        if ($.inArray($target.attr('id'), listIDTime) != -1) {
            getTableData();
        }
    }).on('click blur keypress', function (e) {
        if (e.type !== "keypress" || e.which === 13)
            $(this).remove();
    });
    $clone.find('option').css('background', '#fff8dc');
    $('body').append($clone);
    $clone.focus();
});
//Multiple Select Custom

//Thêm tags
$(document).on('click', '#menu-food> li.food', function () {
    var value = $(this).attr("data-value");
    var text = $(this).text();
    var newli = '<li data-value="' + value + '" id="' + value + '">' +
            text +
            '<button type="button" class="exit-x" data-food-id="' + value + '" data-food-name="' + text + '">' +
            '<i class="fa fa-close"></i>' +
            '</button>' +
            '</li>';
    $('.menu-food-selected > ul').append(newli);
    //Animation khi add Tags (Đặt display none cho thẻ li)
//    $('.menu-food-selected > ul > li[data-value="'+ value +'"]').show('fast');
    $(this).remove();
    $('#select-food').append('<option value="' + value + '" selected>' + text + '</option>');
});
//Xóa Tag
$(document).on('click', '.menu-food-selected >ul > li > .exit-x', function () {
    var id = $(this).attr('data-food-id');
    var name = $(this).attr('data-food-name');
    $('.menu-food-selected > ul').find('#' + id).remove();
    $('#menu-food').append('<li data-value="' + id + '" class="food">' + name + '</li>');
    $('#select-food').find('option[value="' + id + '"]').remove();
});
// Lấy dữ liệu
//Thông tin khách hàng
function getCustomer() {
    var name = $('#name').val();
    var email = $('#email').val();
    var phone = $('#phone').val();
    var note = $('#note').val();
    var customer = {};
    customer.name = name;
    customer.email = email;
    customer.phone = phone;
    customer.note = note;
    return customer;
}

//Ngày giờ

function checkDate() {
    var day = $('#day').val();
    var month = $('#month').val();
    var year = $('#year').val();
    var hour = $('#hour').val();
    var minute = $('#minute').val();
    if (day != "" && month != "" && year != "" && hour != "" && minute != "") {
        return true;
    } else {
        return false;
    }
}
function getDateTime() {
    var day = $('#day').val();
    var month = $('#month').val();
    var year = $('#year').val();
    var hour = $('#hour').val();
    var minute = $('#minute').val();
    if (day != "" && month != "" && year != "" && hour != "" && minute != "") {
        var dateTime = day + "-" + month + "-" + year + ":" + hour + ":" + minute;
        return dateTime;
    } else {
        return false;
    }
}
//Load dữ liệu sau khi chọn Restaurant
var getCompanyData = function () {
    var request = $('#restaurant').val();
    if (request == "") {
        return;
    }
    $('.form-group.table > .se-pre-con').css("display", "block");
    $.ajax({
//        url: "http://192.168.86.30:8888/book/ajax/c/" + request,
        url: origin + "/book/ajax/c/" + request,
        type: "GET",
        dataType: "json",
        success: function ($result) {
            if ($result.status) {
                resetForm();
                bindData($result);
                $('.form-group.table > .se-pre-con').fadeOut('fast');
            } else {
                alert("Error! Please Try Again!");
                $('.form-group.table > .se-pre-con').fadeOut('fast');
            }
        },
        error: function ($result) {
            alert($result);
        }
    });
};
//Bind Data cho form chọn bàn
var bindData = function ($result) {
    var close_time = coverTime($result.data.close_time);
    var open_time = coverTime($result.data.open_time);
    var food_list = $result.data.services_list;
    setHour(open_time, close_time);
    setFood(food_list);
    companyInfo = $result.data;
};
//Gán giờ vào select
var setHour = function (open_time, close_time) {
    for (var i = open_time[0]; i < close_time[0]; i++) {
        if (i < 10 && i > open_time[0]) {
            i = '0' + i;
        }
        $('select#hour').append("<option value='" + i + "'>" + i + "</option>");
    }
    $('select#minute').append('<option value="00">00</option>');
    $('select#minute').append('<option value="15">15</option>');
    $('select#minute').append('<option value="30">30</option>');
    $('select#minute').append('<option value="45">45</option>');
};
//Gán food vào Menu
var setFood = function (food_list) {
    $.each(food_list, function (i, item) {
        $('.menu-food').append('<li data-value="' + item.value + '" class="food">' + item.name + '</li>');
    });
};
//Tách chuỗi hh:mm thành từng số riêng
function coverTime(time) {
    var timeString = time;
    var timeArray = timeString.split(":");
    return timeArray;
}
;
//Reset Data khi chọn lại Nhà Hàng
function resetForm() {
    $('#hour').html("<option value='' style='display:none'>hh</option>");
    $('#minute').html("<option value='' style='display:none'>mm</option>");
    $('#table').html("");
    $('#table').attr("disabled", true);
    $('#menu-food, .menu-food-selected > ul, #select-food').html("");
}
function resetTable() {
    $('#table').html("");
}

//Sự kiện Load dữ liệu Table khi chọn xong ngày giờ
//Dữ liệu trả về chỉ gồm Mã bàn (Cần bàn thêm)
function getTableData() {
    var dateString = getDateString();
    if (dateString == false) {
        return;
    }
    var request = $('#restaurant').val();
    if (request == "") {
        return;
    }
    resetTable();
    $.ajax({
        url: origin + "/book/ajax/p/" + request + "/" + dateString,
        type: "GET",
        dataType: "json",
        success: function ($result) {
            if ($result.status) {
                $.each($result.data, function (i, item) {
                    $('#table').append('<option value="' + item.value + '">' + item.name + '</option>');
                });
                $('#table').removeAttr('disabled');
            } else {
                alert("Error! Please Try Again!");
            }
        },
        error: function ($result) {
            alert($result);
        }
    });
}
//Lấy chuỗi DateString
function getDateString() {
    var hh = $('#hour').val();
    var mm = $('#minute').val();
    var d = $('#day').val();
    var m = $('#month').val();
    var y = $('#year').val();
    if (hh != "" && mm != "" && d != "" && m != "" && y != "") {
        var dateString = y + '-' + m + '-' + d + ':' + hh + ':' + mm;
        return dateString;
    } else {
        return false;
    }
}

//Kiểm tra định dạng Email + Phone
function validate() {
    var result = true;
    var nofication = '';
    //Name
    var name = $.trim($('#name').val());
    if (name == '') {
        nofication = '<p><strong>Name</strong> can not be blank!</p>';
        result = result && false;
    }

//Email
    var email = $.trim($('#email').val());
    var emailReg = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (email != '') {
        if (!emailReg.test(email)) {
            nofication = '<p><strong>Email</strong> in invalid format!</p>';
            result = result && false;
        }
    } else {
        nofication += '<p><strong>Email</strong> can not be blank!</p>';
        result = result && false;
    }

//Phone
    var phone = $.trim($('#phone').val());
    phone = phone.replace('(+84)', '0');
    phone = phone.replace('+84', '0');
    phone = phone.replace('0084', '0');
    phone = phone.replace(/ /g, '');
    if (phone != '') {
        if (phone.match(/^\d{10}/)) {
            result = result && true;
        } else {
            nofication += '<p><strong>Phone</strong> in invalid format!</p>';
            result = result && false;
        }
    } else {
        nofication += '<p><strong>Phone</strong> can not be blank!</p>';
        result = result && false;
    }
    var food = $('#select-food').val().length;
    if (food == 0) {
        result = result && false;
        nofication += '<p>You must choice at least <strong>one dish</strong>!</p>';
    }
    var valiDate = validateDate();
    if (valiDate.status == false) {
        result = result && false;
        nofication += valiDate.nofication;
    }
    if (result != true) {
        popup('#errorModal', nofication);
    }
    return result;
}
//Hiện thông báo xác nhận khi nhấn button send
function showConfirmModal() {
    setConfirmModal();
    $('#confirmModal').modal('show');
}
$('#confirmModal').on('show.bs.modal', function () {
//Nếu người dùng click vào OK thì sẽ tiếp tục submit form
    $('#confirmModal #modal-btn-yes').click(function () {
        $('#booking_date_time_start').val($('#day').val() + '/' + $('#month').val() + '/' + $('#year').val() + ' ' + $('#hour').val() + ':' + $('#minute').val());
        $('form').unbind('submit').submit();
    });
});
function setConfirmModal() {
    $('#confirmModal label.restaurant').html($('#restaurant').val());
    $('#confirmModal .name-label').html($('#name').val());
    $('#confirmModal .email-label').html($('#email').val());
    $('#confirmModal .phone-label').html($('#phone').val());
    $('#confirmModal .time-label').html($('#day').val() + '/' + $('#month').val() + '/' + $('#year').val() + ' ' + $('#hour').val() + ':' + $('#minute').val());
    $('#confirmModal .table-label').html($('#table option[value="' + $('#table').val() + '"]').html());
    $('#confirmModal .food-label').html(getFoodList());
}

//Lấy danh sách thức ăn
function getFoodList() {
    var arr = [];
    $('#select-food > option[selected]').each(function () {
        arr.push($(this).html());
    });
    var string = arr.join(", ");
    return string;
}

//Event trước khi Form Submit 
$('form').submit(function (event) {
    event.preventDefault();
    if (!(validate())) {
        return;
    }
    showConfirmModal();
});
//Kiểm tra thời gian đã chọn là thời gian trong tương lai

//Kiểm tra giờ chọn nằm trong giờ khách sạn mở cửa

//Ngày được đặt phải là cùng hoặc sau ngày hiện tại

function validateDate() {
    var d = $('#day').val();
    var m = $('#month').val();
    var y = $('#year').val();
    var hh = $('#hour').val();
    var mm = $('#minute').val();
    var day = new Date(m + '-' + d + '-' + y + ' ' + hh + ':' + mm);
    var now = new Date();
    var arr = {
        "status": true,
        "nofication": ""
    };
    //30 ngày sau
    var next30day = new Date();
    next30day = new Date(next30day.setDate(next30day.getDate() + 30));
    if (now.getTime() > day.getTime()) {
        //Ngày đặt lịch là ngày trước đó
        arr.nofication = "<p><strong>Date</strong> is invalid! Can't book time in past!</p>";
        arr.status = false;
        return arr;
    } else {
        //Ngày đặt lịch là ngày hôm nay hoặc sau đó
        if ((day.getTime() - now.getTime()) < 3600000) {
            //Thời gian từ lúc đặt tới giờ đặt nhỏ hơn 1 giờ
            arr.nofication = "<p>Book early <strong>at least an hour!</strong> </p>";
            arr.status = false;
            return arr;
        } else {
            //Chỉ được đặt lịch trước tối đa 30 ngày
            if (day.getTime() > next30day.getTime()) {
                arr.nofication = "<p>Book early <strong>up to 30 days</strong></p>";
                arr.status = false;
                return arr;
            }
        }
    }
    return arr;
}